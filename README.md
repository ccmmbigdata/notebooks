![Ciencias Mariñas de Galicia](https://ccmmbigdata.cesga.es/static/images/public/c_logo.png "Ciencias Mariñas de Galicia")

# Plataforma BigData do Programa de Ciencias Mariñas de Galicia
Benvido/a ao repositorio de notebooks compartido do **Programa de Ciencias Mariñas de Galicia**.

Este espazo está deseñado para que os usuarios poidan colaborar e compartir os seus proxectos e análises de datos realizados mediante notebooks de Jupyter.
O repositorio contén notebooks de exemplo que servirán de axuda á hora de comezar a utilizar a **Plataforma BigData** do Programa de Ciencias Mariñas de Galicia.

## Propósito
O propósito deste repositorio é fomentar a colaboración, a aprendizaxe e o intercambio de coñecementos relacionados coa ciencia e análise de datos mariños a través de notebooks de Jupyter.

## Usando os Notebooks
Para comezar a utilizar os notebooks o máis doado é utilizar o portal de Ciencias Mariñas:
- [Portal - Plataforma Big Data Ciencias Mariñas](https://ccmmbigdata.cesga.es/)

## Compartindo Notebooks co resto da comunidade
Se creas un Notebook e queres compartilo co resto da comunidade fainos un ```merge request```.

## Como facer Merge Requests
Se desexas contribuír a este repositorio, segue estes pasos para facer un **Merge Request** de maneira ordenada:

1. **Fork do repositorio:** Fai un fork deste repositorio na túa conta de GitLab facendo clic no botón "Fork" na parte superior dereita desta páxina.
2. **Clona o repositorio:** Clona o teu fork á túa máquina local co comando `git clone git@gitlab.com:ccmmbigdata/notebooks.git`.
3. **Crea unha rama:** Crea unha rama na que traballar co comando `git checkout -b rama`.
4. **Realiza os teus cambios:** Agrega ou modifica os teus Notebooks de Jupyter.
5. **Fai Commit:** Fai commit dos teus cambios con `git commit -m "Mensaxe descritiva"`.
6. **Fai Push:** Sobe os teus cambios ao teu fork en GitLab con `git push origin rama`.
7. **Crea un Merge Request:** Ve á páxina do teu fork en GitLab e presiona o botón "New Merge Request". Asegúrache de proporcionar unha descrición detallada da túa contribución.
8. **Revisión e aprobación:** Os colaboradores do proxecto revisarán o teu Merge Request e poden solicitar cambios ou aprobalo directamente.
9. **Mantén a túa rama actualizada:** Se hai cambios na rama principal do repositorio orixinal, asegúrache de manter a túa rama actualizada con `git pull origin main`.
10. **Limpa a túa rama:** Unha vez que a túa Merge Request sexa aceptado, podes eliminar a rama que creaches.

Esperamos que esta guía axúdeche a contribuír de maneira efectiva a este repositorio.
Se tes algunha pregunta ou necesitas axuda, non dubides en crear un issue ou contactarnos.

## Normas de uso correcto
Para manter unha contorna colaborativa e respectuosa, pedímosche que sigas estas normas de uso:
1. **Respecto e cortesía:** Respecta aos demais colaboradores. Comentarios despectivos, ofensivos ou discriminatorios non serán tolerados.
2. **Contido apropiado:** Asegúrache de que o contido dos teus notebooks sexa apropiado e cumpra coas normas básicas.
3. **Documentación:** Proporciona unha documentación clara e concisa nos teus notebooks. Explica o teu traballo, o contexto e como outros poden utilizalo.

## Documentación adicional
A plataforma BigData do CESGA está baseada nas tecnoloxías Hadoop e Spark, podes encontrar información detallada en:
- [Plataforma BigData CESGA](https://bigdata.cesga.gal)
- [Curso Spark](https://github.com/javicacheiro/pyspark_course)
